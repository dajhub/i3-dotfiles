# i3-dotfiles

## Intro
These are my personal dotfiles for i3, using polybar.  These are for personal use but feel free to use.

![](assets/i3-light.png)

## Copying Files, Folders & Wallpapers 

Uses **Stow** to create the symlinks

In a terminal:

```bash
git clone https://gitlab.com/dajhub/i3-dotfiles
```

Before creating the symlinks with **stow** and preventing it from failing you might want to firstly delete two files in .config: *kitty* and *i3*. In a terminal:

```bash
cd
cd .config
rm -rf kitty i3
cd i3-dotfiles
stow .
```

To install the SDDM theme and wallpapers:

```bash
cd i3-dotfiles
./install.sh
```

## Connect to Wi-Fi

In a terminal to identify Wi-Fi:

```bash
nmcli dev wifi
```

To connect (c) to identified Wi-Fi:

```bash
nmcli device wifi c <<SSID>> password <<PASSWORD>>
```

## i3 Shortcuts

W = windows key

| Keyboard Shortcut     | Result                    |
| ---                   | ---                       |
| W+Return              | starts kitty              |
| W+w                   | starts firefox            |
| W+d                   | starts dolphin    |
| ---                   | ---                       |
| W+q                   | kill focused window       |
| W+space               | Rofi application search   |
| W+t                   | Rofi window switcher      |
| W+Shift+e             | Rofi exit-menu            |
| W+l                   | Lock the system           |
| ---                   | ---                       |
| W+Shift+c             | reload the config file    |
| W+Shift+r             | restart i3 inplace        |
| ---                   | ---                       |
| W+Left                | change window focus left  |
| W+Down                | change window focus down  |
| W+Up                  | change window focus up    |
| W+Right               | change window focus right |
| ---                   | ---                       |
| W+Shift+Left          | move window focus left    |
| W+Shift+Down          | move window focus down    |
| W+Shift+Up            | move window focus up      |
| W+Shift+Right         | move window focus right   |
| ---                   | ---                       |
| W+h                   | split in horiz orientation|
| W+v                   | split in vert orientation |
| ---                   | ---                       |
| W+f                   | enter fullscreen mode     |
| W+Shift+space         | toggle tiling / floating  |
| W+right mouse button  | resize floating window    |