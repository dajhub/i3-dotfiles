#!/bin/bash

#echo "###############################################"
#echo '# SDDM Theming ...'
#echo "###############################################"

# Install corners theme (assumes sddm already installed):
#cd
#git clone https://github.com/aczw/sddm-theme-corners.git
#cd sddm-theme-corners/
#sudo mkdir -p /usr/share/sddm/themes
#sudo cp -r corners/ /usr/share/sddm/themes/
#cd
#rm -rf sddm-theme-corners

# Copies across the sddm theme corners - https://github.com/aczw/sddm-theme-corners
# Makes it the default theme
#echo "[Theme]
#Current=corners" | sudo tee /etc/sddm.conf
#sudo mkdir -p /usr/share/sddm/themes/corners
#sudo cp -f ~/i3-dotfiles/sddm-theme/theme.conf /usr/share/sddm/themes/corners/
#sudo mkdir -p /usr/share/sddm/themes/corners/backgrounds
#sudo cp -f ~/i3-dotfiles/sddm-theme/HadriansWallUK.jpg /usr/share/sddm/themes/corners/backgrounds/

echo "###############################################"
echo '# Copying Wallpapers across ...'
echo "###############################################"

cd
mkdir -p Pictures
git clone https://gitlab.com/dajhub/wallpapers
cd wallpapers
cp -r ~/wallpapers ~/Pictures/
cd
cd Pictures/wallpapers
rm README.md
cd
rm -rf wallpapers


echo "###############################################"
echo '# Finished...'
echo "###############################################"
